const fs = require('fs');
const qrcode = require('qrcode-generator')

const typeNumber = 0
const errorCorrectionLevel = 'L'

const render = (data) => {
  const qr = qrcode(typeNumber, errorCorrectionLevel)
  qr.addData(data)
  qr.make()

  const rawData = qr.createDataURL(10, 0)
  const base64 = rawData.split(';base64,').pop()

  return {
    getRawData: () => rawData,
    getBase64: () => base64,
    getData: () => Buffer.from(base64, 'base64')
  }
}

const run = () => {

  for(let i=1;i<= 150; i++) {
    const id = i.toString().padStart(3, '0')
    const key = `F${id}`

    const x = render(`https://intageth.typeform.com/to/tPl4jG?phone_number=${key}`)
    fs.writeFileSync(`output/${key}.gif`, x.getData())
  }


}



run()
